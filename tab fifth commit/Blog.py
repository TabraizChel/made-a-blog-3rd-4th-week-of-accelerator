''' will open a txt file called content and write the content 'text' into the file
    this will store all the blogs in one file each seperated by'''
def draftContent(text):
    draft = open("content.txt", 'a')
    draft.write(text)
    draft.write('-----')
    draft.close()


''' will open a txt file called title and write the content 'title' into the file
    this will store all the blog titles in one file each seperated by '''
def draftTitle(title):
    Title = open("title.txt", 'a')
    Title.write(title)
    Title.write('-----')
    Title.close()

''' Makes a dictionary of title:blog-content.
    It takes a title and returns the blog associated with that title'''
def entriesDict(title):
    text_entry = entriesList('content.txt')
    title_entry = entriesList('title.txt')
    blogDict = {}
    for i in range(len(text_entry)):
        blogDict[title_entry[i]] = text_entry[i]
    return blogDict[title]



'''converts entries in a text file (file) into a list'''
def entriesList(file):
    a = open(file).read().split('-----')
    a.pop()
    return a


'''Checks if the title is already used by another blog post
    if so returns false'''
def titleCheck(title):
    entries = entriesList('title.txt')
    for entry in entries:
        if title == entry:
            return False



def draftAccess(entry):
    text_entry = open('content.txt').read().split('-----')
    text_entry.pop()
    return text_entry[entry]
