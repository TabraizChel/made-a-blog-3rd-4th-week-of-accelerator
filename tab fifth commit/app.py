from flask import Flask, render_template, request
import Blog
app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def index():

        return render_template('home.html' )


@app.route('/sub', methods=['GET', 'POST'])
def sub():
        return render_template('submissions.html', Title_in_use = '' )


@app.route('/results', methods=['GET', 'POST'])
def results():
    title = request.form["title"]
    if  Blog.titleCheck(title) == False:
        return render_template('submissions.html', Title_in_use = 'Title already in use' )
    else:
        Blog.draftTitle(title)

        text = request.form["text"]
        Blog.draftContent(text)
        return render_template('submissions.html', Title_in_use = '' )



@app.route('/entry', methods=['GET', 'POST'])
def entry():
        titles_in_list = Blog.entriesList('title.txt')
        entry = request.form["search"]
        if entry == '1':
            content = Blog.entriesDict(titles_in_list[0])
            return render_template('Comments_and blog2.html', Text=content, Title=titles_in_list[0])
        elif entry == '2':
            content = Blog.entriesDict(titles_in_list[1])
            return render_template('Comments_and blog2.html', Text=content, Title=titles_in_list[1])
        elif entry == '3':
            content = Blog.entriesDict(titles_in_list[2])
            return render_template('Comments_and blog2.html', Text=content, Title=titles_in_list[2])
        else:
            content = Blog.entriesDict(entry)
            return render_template('Comments_and blog2.html', Text=content, Title=entry)




if __name__ == '__main__':
     app.run()
